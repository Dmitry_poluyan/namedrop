var Mail = require('../libs/mail');

module.exports.invitePeople = function (req, res, next) {
  var options = {
    from: req.user.email,
    to: req.body.sendTo,
    subject: req.body.subject,
    text: req.body.text
  };

  return Mail
    .sendMail(options)
    .then(function (result) {
      return res.json(result);
    })
    .catch(next);
};
