'use strict';

var _ = require('lodash');

var UserModel = require('../models/user');
var UploadFile = require('../libs/uploadFile')('directory');
var Converter = require('../libs/converter');
var FileManger = require('../libs/fileManager');

//#Simple user
module.exports.updateUser = function (req, res, next) {
  return UserModel
    .update(
      {_id: req.user._id},
      {$set: _.pick(req.body, ['firstName', 'lastName'])}
    )
    .then(function (modification) { //modification: {n:Number, nModified:Number, ok:Number}
      return res.json({status: 'OK', modification: modification});
    })
    .catch(next);
};

//*Record
module.exports.updateRecordForUser = function (req, res, next) {
  return UploadFile
    .saveFile(req.file)
    .then(deleteOldFileIfExist)
    .then(updateUserRecord)
    .catch(next);

  function deleteOldFileIfExist(newRecordId) {
    if (req.user.recordId) {
      return UploadFile
        .deleteFile(req.user.recordId)
        .then(function () {
          return newRecordId;
        }, function (error) {
          if (error.message && error.message === 'Not Found') {
            return newRecordId;
          }
          throw error;
        });
    }
    return newRecordId;
  }

  function updateUserRecord(newRecordId) {
    return UserModel
      .update(
        {_id: req.user._id},
        {$set: {recordId: newRecordId}}
      )
      .then(function (modification) { //modification: {n:Number, nModified:Number, ok:Number}
        return res.json({status: 'OK', modification: modification});
      });
  }
};

module.exports.getUserRecord = function (req, res, next) {
  if (!req.user.recordId) {
    var error = new Error('Not found');
    error.status = 404;
    return next(error);
  }

  return UploadFile
    .getFile(req.user.recordId, res)
    .then(function () {
      return res.end();
    })
    .catch(next);
};

//*Csv
module.exports.sendCSV = function (req, res, next) {

  var csvOptions = {
    fields: ['lastName', 'firstName', 'role', 'provider'],
    fieldNames: ['Last name', 'First name', 'Role', 'Provider'],
    quotes: ''
  };

  return Converter
    .jsonToCsv(req.user, csvOptions)
    .then(FileManger.writeFile)
    .then(sendFile)
    .then(FileManger.deleteFile)
    .catch(next);

  function sendFile(filePath) {
    return new Promise(function (resolve, reject) {
      return res.download(filePath, function (err) {
        if (err) {
          return reject(err);
        }
        return resolve(filePath);
      });
    });
  }
};


//#Common
module.exports.getRecordByUserId = function (req, res, next) {
  return UserModel
    .findById(req.params.userId)
    .then(isExists)
    .then(sendFile)
    .catch(next);

  function isExists(user) {
    if (!user) {
      var error = new Error('Not found');
      error.status = 404;
      return next(error);
    }
    return user.recordId;
  }

  function sendFile(recordId) {
    return UploadFile
      .getFile(recordId, res)
      .then(function () {
        return res.end();
      });
  }
};

//#Admin
module.exports.createNewUser = function (req, res, next) {
  var newUser = new UserModel({
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    local: {
      email: req.body.email,
      password: newUser.generateHash(req.body.password)
    },
    role: req.body.role
  });

  return newUser
    .save()
    .then(function (newUser) {
      return res.status(201).json({status: 'OK', user: newUser});
    })
    .catch(next);
};

module.exports.updateUserById = function (req, res, next) {
  return UserModel
    .update(
      {_id: req.params.userId},
      {$set: _.pick(req.body, ['firstName', 'lastName'])}
    )
    .then(function (modification) { //modification: {n:Number, nModified:Number, ok:Number}
      return res.json({status: 'OK', modification: modification});
    })
    .catch(next);
};

module.exports.getAllUsers = function (req, res, next) {
  return UserModel
    .find({})
    .then(function (users) {
      return res.json({status: 'OK', users: users});
    })
    .catch(next);
};

module.exports.getUserById = function (req, res, next) {
  return UserModel
    .findById(req.params.userId)
    .then(isExists)
    .then(function (user) {
      return res.json({status: 'OK', user: user});
    })
    .catch(next);

  function isExists(user) {
    if (!user) {
      var err = new Error('Not found');
      err.status = 404;
      return next(err);
    }
    return user;
  }
};

module.exports.deleteUserById = function (req, res, next) {
  return UserModel
    .findById(req.params.userId)
    .then(isExists)
    .then(deleteRecordFileIfExist)
    .then(deleteUser)
    .catch(next);

  function isExists(user) {
    if (!user) {
      var err = new Error('Not found');
      err.status = 404;
      return next(err);
    }
    return user;
  }

  function deleteRecordFileIfExist(user) {
    if (user.recordId) {
      return UploadFile
        .deleteFile(user.recordId)
        .then(function () {
          return user;
        }, function (error) {
          if (error.message && error.message === 'Not Found') {
            return user;
          }
          throw error;
        });
    }
    return user;
  }

  function deleteUser(user) {
    return UserModel
      .remove({_id: user._id})
      .then(function (modification) {
        return res.json({status: 'OK', modification: modification});
      });
  }

};