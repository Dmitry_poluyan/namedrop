'use strict';

var _ = require('lodash');

//#Auth
module.exports.logout = function (req, res) {
  req.logout();
  return res.json({status: 'Logout'}); //TODO: What need return?
};

module.exports.getSessionUser = function (req, res) {
  return res.status(200).json({status: 'Authorized', user: _.omit(req.user, ['local'])}); //TODO: What need return?
};