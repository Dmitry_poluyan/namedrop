'use strict';
var bcrypt = require('bcrypt');
var mongoose = require('mongoose');

mongoose.Promise = require('bluebird');

var Schema = mongoose.Schema;

var User = new Schema({ //fixme: validation + what about structure scheme?
  local: {
    email: {
      type: String,
      unique: true,
      lowercase: true,
      trim: true
    },
    password: {
      type: String
    }
  },
  facebook: {
    id: {
      type: String
    },
    token: {
      type: String
    },
    email: {
      type: String,
      lowercase: true,
      trim: true
    }
  },
  twitter: {
    id: {
      type: String
    },
    token: {
      type: String
    },
    userName: {
      type: String,
      trim: true
    }
  },
  linkedin: {
    id: {
      type: String
    },
    token: {
      type: String
    },
  },
  firstName: {
    type: String,
    trim: true
  },
  lastName: {
    type: String,
    trim: true
  },
  provider: {
    type: String,
    required: true,
    trim: true
  },
  role: {
    type: String,
    required: true,
    trim: true
  },
  recordId: String
});

User.methods.generateHash = function (password) {
  return bcrypt.hashSync(password.trim(), bcrypt.genSaltSync(9));
};

User.methods.validPassword = function (password) {
  return bcrypt.compareSync(password, this.local.password);
};

module.exports = mongoose.model('User', User);