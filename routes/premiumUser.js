'use strict';

var express = require('express');

var checkAuth = require('../libs/checkAuth');
var log = require('../libs/log')(module);
var acl = require('../libs/acl');

var router = express.Router();

router.use(function timeLog(req, res, next) {
  log.info('Time: ', Date.now());
  next();
});

//#Middleware
router.use(checkAuth);
router.use(acl.authorize);

//#For test
router.get('/test', function (req, res) {
  return res.json({mes: 'premium test'});
});

module.exports = router;