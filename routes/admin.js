'use strict';

var express = require('express');

var UserCtrl = require('../controllers/user');
var checkAuth = require('../libs/checkAuth');
var log = require('../libs/log')(module);
var acl = require('../libs/acl');

var router = express.Router();

router.use(function timeLog(req, res, next) {
  log.info('Time: ', Date.now());
  next();
});

//#Middleware
router.use(checkAuth);
router.use(acl.authorize);

//#Routes
//*User
router.get('/user', UserCtrl.getAllUsers);
router.get('/user/:userId', UserCtrl.getUserById);

router.post('/user', UserCtrl.createNewUser);

router.put('/user/:userId', UserCtrl.updateUserById);

router.delete('/user/:userId', UserCtrl.deleteUserById);

module.exports = router;