'use strict';

var express = require('express');
var multer  = require('multer');

var UserCtrl = require('../controllers/user');
var MailCtrl = require('../controllers/mail');
var checkAuth = require('../libs/checkAuth');
var log = require('../libs/log')(module);
var acl = require('../libs/acl');
var upload = multer({dest: 'temporaryFileStorage/'});
var checkFileType = require('../libs/checkFileType');

var router = express.Router();

router.use(function timeLog(req, res, next) {
  log.info('Time: ', Date.now());
  next();
});

//#Middleware
router.use(checkAuth);
router.use(acl.authorize);

//#Routes
router.get('/record', UserCtrl.getUserRecord);
router.get('/invite', MailCtrl.invitePeople);
router.get('/csv', UserCtrl.sendCSV);

router.put('/', UserCtrl.updateUser);
router.put('/record', upload.single('file'), checkFileType.isRecord, UserCtrl.updateRecordForUser);

module.exports = router;