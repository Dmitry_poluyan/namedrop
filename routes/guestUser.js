'use strict';

var express = require('express');

var log = require('../libs/log')(module);
var UserCtrl = require('../controllers/user');

var router = express.Router();

router.use(function timeLog(req, res, next) {
  log.info('Time: ', Date.now());
  next();
});

//#Routes
router.get('/record/:userId', UserCtrl.getRecordByUserId);

module.exports = router;