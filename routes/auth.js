'use strict';

var passport = require('passport');
var express = require('express');

var AuthCtrl = require('../controllers/auth.js');
var log = require('../libs/log')(module);
var checkAuth = require('../libs/checkAuth');

var router = express.Router();

router.use(function timeLog(req, res, next) {
  log.info('Time: ', Date.now());
  next();
});


//Local
router.post('/login', passport.authenticate('local-login'),
  function (req, res) {
    return res.json({status: 'OK'});//todo: What need return?
  }
);
router.post('/signup', passport.authenticate('local-signup'),
  function (req, res) {
    return res.json({status: 'OK'});//todo: What need return?
  }
);


//Facebook
router.get('/facebook', passport.authenticate('facebook'));
router.get('/facebook/callback', passport.authenticate('facebook'),
  function (req, res) {
    return res.json({status: 'OK'}); //todo: What need return?
  }
);


//Twitter
router.get('/twitter', passport.authenticate('twitter'));
router.get('/twitter/callback', passport.authenticate('twitter'),
  function (req, res) {
    return res.json({status: 'OK'}); //todo: What need return?
  }
);


//LinkedIn
router.get('/linkedin', passport.authenticate('linkedin'));
router.get('/linkedin/callback', passport.authenticate('linkedin'),
  function (req, res) {
    return res.json({status: 'OK'}); //todo: What need return?
  }
);


//Other
router.get('/user/me', checkAuth, AuthCtrl.getSessionUser);
router.get('/logout', AuthCtrl.logout);

module.exports = router;