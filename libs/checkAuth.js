'use strict';

module.exports = function (req, res, next) {
  if (req.isAuthenticated()) {
    req.decoded = req.user;
    return next();
  }
  return res.status(401).json({status: 'Unauthorized'});
};