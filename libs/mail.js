var Promise = require('bluebird');
var config = require('../config');
var mailgun = require('mailgun-js')({apiKey: config.get('mailgun:api_key'), domain: config.get('mailgun:domain')});

module.exports.sendMail = function (data) {
  var options = {
    from: data.email,
    to: data.sendTo,
    subject: data.subject,
    text: data.text
  };

  return new Promise(function (resolve, reject) {
    return mailgun
      .messages()
      .send(options, function (error, body) {
        if (error) {
          return reject(error);
        }
        return resolve(body);//Do smth
      });
  });
};
