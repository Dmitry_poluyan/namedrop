'use strict';

var fs = require('fs');

module.exports.isRecord = function (req, res, next) {
  if(req.file){
    if (req.file.mimetype === 'audio/mp3') {
      return next();
    }
    var filePath = req.file.path;

    return fs.stat(filePath, function (error, stats) {
      if (error || !stats.isFile()) {
        error = new Error('Not found');
        error.status = 404;
        return next(error);
      }
      return fs.unlink(filePath, function (error) {
        if (error) {
          return next(error);
        }
        error = new Error('Unsupported Media Type');
        error.status = 415;
        return next(error);
      });
    });
  }
  var error = new Error('Not found');
  error.status = 404;
  return next(error);
};