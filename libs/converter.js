"use strict";

var Promise = require('bluebird');
var json2csv = require('json2csv');

module.exports.jsonToCsv = function (data, options) {

  var opts = {
    data: data,
    fields: options.fields || '',
    fieldNames: options.fieldNames || '',
    quotes: options.quotes || ''
  };
  return new Promise(function (resolve, reject) {
    return json2csv(opts, function (error, csv) {
      if (error) {
        return reject(error);
      }
      return resolve({content:csv, type:'csv'});
    });
  });
};