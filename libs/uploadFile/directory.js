'use strict';
var fs = require('fs');
var mimeType = require('mime');
var uuid = require('node-uuid');
var path = require('path');

var Promise = require('bluebird');
Promise.promisifyAll(fs);

fs.existsAsync = Promise.promisify(
  function (path, existsCallback) {
    return fs.exists(path, function (exists) {
      return existsCallback(null, exists);
    });
  }
);

var directoryPath = 'fileStorage/';
var pathCache = {};

function FileUpload() {

  this.setConfig = function (config) {
    directoryPath = config.dest;
  };

  this.saveFile = function (file) {
    return new Promise(function (resolve, reject) {
      var fileId = uuid();
      var oldPath = file.path;
      var newPath = directoryPath + fileId + path.extname(file.originalname);

      return fs
        .existsAsync(directoryPath)
        .then(function (exist) {
          if (!exist) {
            return fs.mkdirAsync(directoryPath);
          }
        })
        .then(function () {
          return fs
            .renameAsync(oldPath, newPath)
            .then(function () {
              pathCache[fileId] = newPath;
              return resolve(fileId);
            });
        })
        .catch(reject);
    });
  };

  this.getFile = function (fileId, res) {
    return new Promise(function (resolve, reject) {
      getFilePath(fileId)
        .then(isFile)
        .then(readAndPipeStream)
        .catch(reject);

      function isFile(filePath) {
        return fs.existsAsync(filePath)
          .then(function (exist) { //{exist:Boolean}
            if (!exist) {
              var err = new Error('Not Found');
              err.status = 404;
              delete pathCache[fileId];
              return reject(err);
            }
            return filePath;
          });
      }

      function readAndPipeStream(filePath) {
        res.setHeader('Content-type', mimeType.lookup(filePath));

        var stream = fs.createReadStream(filePath);

        stream.on('error', function (err) {
          return reject(err);
        });

        stream.on('end', function () {
          return resolve({status: 'OK'});
        });

        stream.pipe(res);

        res.on('close', function () {
          stream.destroy();
        });
      }
    });
  };

  this.deleteFile = function (fileId) {
    return new Promise(function (resolve, reject) {
      return getFilePath(fileId)
        .then(isFileExists)
        .then(deleteFile)
        .catch(reject);

      function isFileExists(filePath) {
        return fs.existsAsync(filePath)
          .then(function (exist) {
            if (!exist) {
              var err = new Error('Not Found');
              err.status = 404;
              delete pathCache[fileId];
              return reject(err);
            }
            return filePath;
          });
      }

      function deleteFile(filePath) {
        return fs.unlinkAsync(filePath)
          .then(function () {
            delete pathCache[fileId];
            return resolve({status: 'OK'});
          });
      }
    });
  };

  function getFilePath(key) {
    return new Promise(function (resolve, reject) {
      if (!pathCache[key]) {
        return searchFilePathById(key)
          .then(function (filePath) {
            if (!filePath) {
              var err = new Error('Not Found');
              err.status = 404;
              return reject(err);
            }
            return resolve(filePath);
          })
          .catch(reject);
      }
      return resolve(pathCache[key]);
    });
  }

  function searchFilePathById(id) {
    return new Promise(function (resolve, reject) {
      return fs.existsAsync(directoryPath)
        .then(isExists)
        .then(findFilePath)
        .catch(reject);

      function isExists(exist) {
        if (!exist) {
          var err = new Error('Not Found');
          err.status = 404;
          return reject(err);
        }
      }

      function findFilePath() {
        return fs.readdirAsync(directoryPath)
          .then(function (list) {
            for (var i = 0; i < list.length; i++) {
              addFilePathToCache(list[i]);
              if (list[i].indexOf(id) > -1) {
                return resolve(directoryPath + list[i]);
              }
            }
            return resolve(null);
          });
      }
    });
  }

  function addFilePathToCache(fileName) {
    var fileId = fileName.split('.')[0];
    if (!pathCache[fileId]) {
      pathCache[fileId] = directoryPath + fileName;
    }
  }
}

module.exports = FileUpload;