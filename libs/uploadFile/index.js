'use strict';

var Directory = require('./directory.js');
var MongoGrid = require('./mongoGrid.js');

var strategies = {
  'mongoGrid': MongoGrid,
  'directory': Directory
};

function SetUploadStrategy(strategy) {
  return new strategies[strategy]();
}

module.exports = SetUploadStrategy;