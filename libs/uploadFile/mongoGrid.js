'use strict';
var mongoose = require('mongoose');
var fs = require('fs');
var Grid = require('gridfs-stream');

var gfs = new Grid(mongoose.connection.db, mongoose.mongo);

var Promise = require('bluebird');
Promise.promisifyAll(fs);

fs.existsAsync = Promise.promisify(
  function (path, existsCallback) {
    return fs.exists(path, function (exists) {
      return existsCallback(null, exists);
    });
  }
);

function UploadInMongoGrid() {

  this.saveFile = function (file) {
    return new Promise(function (resolve, reject) {
      var filePath = file.path;

      return fs.existsAsync(filePath)
        .then(isFile)
        .then(readAndWriteFileToMongoGrid)
        .catch(reject);

      function isFile(exist) {
        if (!exist) {
          var err = new Error('Not found');
          err.status = 404;
          return reject(err);
        }
        return filePath;
      }

      function readAndWriteFileToMongoGrid(filePath) {
        var fileId = new mongoose.mongo.ObjectID();

        var readStream = fs.createReadStream(filePath);
        readStream.on('error', function (err) {
          return reject(err);
        });

        var wrireStream = gfs.createWriteStream({
          _id: fileId,
          filename: file.originalname,
          mode: 'w',
          content_type: file.mimetype
        });

        wrireStream.on('error', function (err) {
          return reject(err);
        });
        wrireStream.on('close', function (file) {
          wrireStream.end();

          return fs.unlinkAsync(filePath)
            .then(function () {
              return resolve(file._id);
            })
            .catch(reject);
        });

        readStream.pipe(wrireStream);
      }
    });
  };

  this.getFile = function (fileId, res) {
    return new Promise(function (resolve, reject) {
      var id = gfs.tryParseObjectId(fileId);

      return gfs.files
        .findOne({_id: id})
        .then(function (file) {
          if (!file) {
            var err = new Error('Not found');
            err.status = 404;
            return reject(err);
          }
          res.setHeader('Content-type', file.contentType);

          var readStream = gfs.createReadStream({_id: file._id});

          readStream.on('error', function (err) {
            return reject(err);
          });
          readStream.on('end', function () {
            return resolve({status: 'OK'});
          });
          res.on('close', function () {
            readStream.destroy();
          });

          readStream.pipe(res);
        })
        .catch(reject);
    });
  };

  this.deleteFile = function (fileId) {
    return new Promise(function (resolve, reject) {
      var id = gfs.tryParseObjectId(fileId);

      return gfs
        .exist({_id: id})
        .then(isFound)
        .then(removeFileFromMongoGrid)
        .catch(reject);

      function isFound(found) {
        if (!found) {
          var err = new Error('Not found');
          err.status = 404;
          return reject(err);
        }
      }

      function removeFileFromMongoGrid() {
        return gfs
          .remove({_id: id})
          .then(function () {
            return resolve({status: 'OK'});
          });
      }
    });
  };
}

module.exports = UploadInMongoGrid;