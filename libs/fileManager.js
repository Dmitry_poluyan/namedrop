"use strict";

var Promise = require('bluebird');
var uuid = require('node-uuid');
var fs = require('fs');

Promise.promisifyAll(fs);

var defaultDirectoryPath = 'temporaryFileStorage/';

module.exports.writeFile = function (data) {

  var directoryPath = data.path ? data.path : defaultDirectoryPath;
  var fileId = uuid();
  var newPath = directoryPath + fileId + '.' + data.type;

  return fs
    .existsAsync(directoryPath)
    .then(createFolderIfNotExist)
    .then(createFile);

  function createFolderIfNotExist(exist) {
    if (!exist) {
      return fs.mkdirAsync(directoryPath);
    }
  }

  function createFile() {
    return new Promise(function (resolve, reject) {
      return fs
        .writeFile(newPath, data.content, function (error) {
          if (error) {
            return reject(error);
          }
          return resolve(newPath);
        });
    });
  }
};

module.exports.deleteFile = function (filePath) {

  return fs
    .existsAsync(filePath)
    .then(isExists)
    .then(deleteFile);

  function isExists(exist) {
    if (!exist) {
      var err = new Error('Not Found');
      err.status = 404;
      throw err;
    }
  }

  function deleteFile() {
    return fs
      .unlinkAsync(filePath)
      .then(function () {
        return {status: 'OK'};
      });
  }
};