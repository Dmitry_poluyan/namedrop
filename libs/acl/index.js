var acl = require('express-acl');

acl.config({
  filename: 'nacl.json',
  path: `${__dirname}`,
  baseUrl: 'api'
});

module.exports = acl;

//For example
// router.use(acl.authorize.unless({
//   path: ['/user/me']
// }));

//   "action": "deny"