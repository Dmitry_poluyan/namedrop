var LocalStrategy = require('passport-local').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
var TwitterStrategy = require('passport-twitter').Strategy;
var LinkedInStrategy = require('passport-linkedin').Strategy;

var User = require('../models/user');
var config = require('../config');

module.exports = function (passport) {

  passport.serializeUser(function (user, done) {
    return done(null, user.id);
  });

  passport.deserializeUser(function (id, done) {
    return User
      .findById(id)
      .then(function (user) {
        return done(null, user);
      })
      .catch(function (error) {
        return done(error);
      });
  });

  passport.use('local-signup', new LocalStrategy({
      usernameField: 'email',
      passwordField: 'password',
      passReqToCallback: true
    },
    function (req, email, password, done) {

      return process.nextTick(function () {

        return User
          .findOne({'local.email': email})
          .then(function (user) {

            if (user) {
              return done(null, false); //'That username already exist' //FIXME: send message
              // return done({message: 'That username already exist', status: 400, name: 'Bad Request'}); //FIXME: status?name? Whate need return?
            }

            var newUser = new User();
            newUser.local = {
              email: email,
              password: newUser.generateHash(password)
            };
            newUser.provider = 'local';
            newUser.role = 'base';
            newUser.firstName = req.body.firstName || '';
            newUser.lastName = req.body.lastName || '';

            return newUser
              .save()
              .then(function (user) {
                return done(null, user);
              });
          })
          .catch(function (error) {
            return done(error);
          });
      });
    })
  );

  passport.use('local-login', new LocalStrategy({
      usernameField: 'email',
      passwordField: 'password',
      passReqToCallback: true
    },
    function (req, email, password, done) {

      return process.nextTick(function () {

        return User
          .findOne({'local.email': email})
          .then(function (user) {
            if (!user) {
              return done(null, false); //'User Not found' //FIXME: send message Whate need return?
              // return done({message: 'That username not found', status: 404, name: 'Not Found'});
            }
            if (!user.validPassword(password)) {
              return done(null, false); //'invalid password' //FIXME: send message
              // return done({message: 'That password is invalid', status: 401, name: 'Unauthorized '}); //FIXME: status?name? Whate need return?
            }
            return done(null, user);
          })
          .catch(function (error) {
            return done(error);
          });
      });
    }
  ));

  passport.use(new FacebookStrategy({
      clientID: config.get('auth:facebookAuth:clientID'),
      clientSecret: config.get('auth:facebookAuth:clientSecret'),
      callbackURL: config.get('auth:facebookAuth:callbackURL'),
      passReqToCallback: true,
      profileFields: ['id', 'name', 'email']
    },
    function (req, accessToken, refreshToken, profile, done) {

      return process.nextTick(function () {
        return User
          .findOne({'facebook.id': profile.id})
          .then(function (user) {

            if (user) {
              return done(null, user);
            }
            var newUser = new User();
            newUser.facebook = {
              id: profile.id,
              token: accessToken,
              email: profile.emails[0].value || ''
            };
            newUser.provider = profile.provider || 'facebook';
            newUser.role = 'base';
            newUser.firstName = profile.name.givenName || '';
            newUser.lastName = profile.name.familyName || '';

            return newUser
              .save()
              .then(function (user) {
                return done(null, user);
              });
          })
          .catch(function (error) {
            return done(error);
          });
      });
    })
  );

  passport.use(new TwitterStrategy({
      consumerKey: config.get('auth:twitterAuth:consumerKey'),
      consumerSecret: config.get('auth:twitterAuth:consumerSecret'),
      callbackURL: config.get('auth:twitterAuth:callbackURL'),
      passReqToCallback: true
    },
    function (req, token, tokenSecret, profile, done) {

      return process.nextTick(function () {

        return User
          .findOne({'twitter.id': profile.id})
          .then(function (user) {
            if (user) {
              return done(null, user);
            }
            var fullName = profile.displayName ? profile.displayName.split(' ') : [];

            var newUser = new User();
            newUser.twitter = {
              id: profile.id,
              token: token,
              userName: profile.username
            };
            newUser.provider = profile.provider || 'twitter';
            newUser.role = 'base';
            newUser.firstName = fullName[0] || '';
            newUser.lastName = fullName[1] || '';

            return newUser
              .save()
              .then(function (user) {
                return done(null, user);
              });
          })
          .catch(function (error) {
            return done(error);
          });
      });
    })
  );

  passport.use(new LinkedInStrategy({
      consumerKey: config.get('auth:linkedInAuth:consumerKey'),
      consumerSecret: config.get('auth:linkedInAuth:consumerSecret'),
      callbackURL: config.get('auth:linkedInAuth:callbackURL'),
      passReqToCallback: true
    },
    function (req, token, tokenSecret, profile, done) {

      return process.nextTick(function () {
        User
          .findOne({'linkedin.id': profile.id})
          .then(function (user) {
            if (user) {
              return done(null, user);
            }

            var newUser = new User();
            newUser.linkedin = {
              id: profile.id,
              token: token
            };
            newUser.provider = profile.provider || 'linkedin';
            newUser.role = 'base';
            newUser.firstName = profile.name.givenName || '';
            newUser.lastName = profile.name.familyName || '';

            return newUser
              .save()
              .then(function (user) {
                return done(null, user);
              });
          })
          .catch(function (error) {
            return done(error);
          });
      });
    })
  );

};